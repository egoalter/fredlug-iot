---
marp: true
author: Peter Larsen - peter@peterlarsen.org
transition: fade

theme: dracula
_class: lead
size: 16:9
paginate: true
backgroundColor: #000
# backgroundImage: "linear-gradient(to bottom, #67b8e3, #0288d1)"
color: white
footer: "IoT and Home Assistant"
style: |
  footer {
    color: yellow;
    text-align: center;
    overflow: hidden;
  }
---

![bg left:40% 80%](esphome_HA_logo.jpg)

# **IoT** and **Home-Assistant** (HASS)

Welcome to FredLUG

Speaker: Peter Larsen, peter@peterlarsen.org

---
<!-- _theme: uncover -->

# Agenda

- Welcome
- IoT - Internet of Things
    - Definition of IoT
    - Devices for IoT
- Home Assistant
    - Automation
    - ESPHome
- Demo: Putting it together

---
<!-- @auto-scaling: true -->

### Mandatory self promotion

![bg right:40% 75%](breadboard.jpg)

- **Name**: Peter Larsen
- **IoT status**: Present
- **Open Source fan**: Absolutely
- **Raspberry Pis**: Many!</br>
- **ICs and PCBs**: Too many to count
- **Osciloscope**: Powered and ready
- **Automation fan**: As much as possible

---

## A perfect marriage

Love of both electronics and computing. 

Open Source even more so. 

---

## BUT ....

IoT does not require an electronics degree. 

Home Assistant will work with commercial as well as self-made devices.

---

# IoT - the definition
<!-- 
_class: lead
_theme: gaia

 -->

## Internet of Things

- Network enabled devices
- Can be LAN, WAN, global - doesn't matter
- Smart devices - have API capabilities
- Controller needed - can be cloud based or run locally (Home Assistant)
- IoT does not require TCP/IP - other network types are used too.

---

# What's a "thing"?

- A MicroController Unit (MCU) connected to sensor and actuators
- MCU has communication features to a central controller
    - But can work indepedent with limited features
- Sensors: A thousand of them (see small sample I have here)
- Actuators: Relays, mosfet switches, sound, motors, steppers, servos, pnumatic etc. etc.

---

# DYI yes - but not required

- Lots of commercial sensors exist that just require a battery
- Alarm systems, cameras, door sensors, motion sensors
    - Ready to run/use by plugging them in, downloading an app and you're off to the races
- Basic electronics knowledge required for DYI - Arduino level
- DYI equipment are often a few $ per MCU and sensors are often less than that per piece. You need soldering, scopes, wires, breadboards and more to design, test before making a PCB.

---

# IoT

- Often misunderstood
    - There are no "smart" devices, it's all computers
- They don't spy on you if you code them yourself
- Buying/trusting 3rd party code is the challenge
- Trusting external services to automate your home? WHY?

---

# IoT Networking

- TCP/IP and internet has made IoT very popular
    - Wide reach
    - Allows for hosted services
- ZWave is a low power very popular alternative
    - Not cheap
- ZigBee also low-power and not much cheaper than ZWave
- Power is everything; not all devices can be plugged into power
    - Battery life in years is not possible with WiFi

---

# mDNS
### Multicast DNS (Avahi)

- IoT often makes use of mDNs
    - So does your network printer, your smart TV, smart phone etc
- Fedora, CentOS etc. comes with Avahi out of the box
- Do need for external DNS
- Can be used to find device types without knowing the name

---
<!-- 
_theme: gaia
_transition: cover
_style: |
    img[alt~="right"] {
      display: block;
      margin: 0 auto;
    }
    section{
      justify-content: flex-start;
    }
 -->

# Device types

- Commercial Devices (Wemo, Ring, GE Digital and a ton more )
    - Often alarms/survaliance type of products
    - Easy to use
    - Requires access to public cloud, registration, tracking etc.
- ESP 8266/32 - Think Arduino, PlatformIO etc.
- zWave/Zigbee

![bg right:25% 100%](wemo-switch.webp)

---
<!--
_style: |
  h1 {
    color: yellow;
    text-align: center;
  }
-->

# My Lab - the DYI approach

![bg vertical](electronics-lab.jpg)
![bg horizontal](fluke.jpg)

---

# My Lab: Software 

- All FOSS (FPGA "programming" is the exception)
    - KiCAD
    - LTspice
    - ESPHome
    - VSCode/PlatformIO
    - avrDude, minipro (chip programming)
- Online resources like Eletrodoc, DigiKey, EveryCircuit, Mouser etc.

---

# My Lab: Hardware

- Lots of MCU types - for IoT mostly ESP type devices.
- ESP-01 are very small, very low power and great for basic devices.
- ESP8266 is a popular but old (meaning limited features) MCU.
- ESP32 is the current "state of the art" MCU with lots of key features, but also a relatively high power consumption even in sleep.
- Arduino's and "blue-pill" (STM) for non wireless communication.
- Raspberry Pi - 2, 3 and 4. 

---

# My Lab: The non MCU hardware

- Full range of resistors, capacitors, BJT/Mosfet and other transistors
- Thru hole for prototypes - SMD for final design.
- Bench Power supplies, Scope, multimeters, frequency generators, DC load simulation, soldering/desoldering equipment etc.
- Too many to count sensors, actuators, motors, steppers, heat/cool, LCD displays and a lot more
- ICs - old tech and new.

---

# ESPHome

To use custom devices, you can:

1. Program them using MCU programmers like Arduino, AVRDUDE
2. Program/define using "ESPHome" (https://esphome.io)

Our demo will use ESPHome with minimum code and complexity

---

# ESPHome in lab

- Programming via IDE and manually adopting them
- Today's demo uses ESPHome plugin with HA
    - Simplify process - less hardware specific
    - Process is "new" to me expect demo failure :D
- PlatformIO is a good in-between the basics of Arduino and professional programming of micro controllers. 
    - Lots of automation and help, still requires good programming

---

# Programming languages

When not using ESPHome

- C/C++
- Assembler
- Register magic (shifting, setting)
- A bit of Python (slow)

---

# ESPHome programming

It's YAML all the way down

```
sensor:
  - platform: aht10
    id: aht
    temperature:
      name: "Temperature Test"
      accuracy_decimals: 1
    humidity:
      name: "Humidity"
      accuracy_decimals: 0
```

---

# The hard way

Offers 100% flexibility, control and features. But requires detailed knowledge of the MCU, hardware communication protocols and FTDI and similar protocols

# The easy way - our choice

Declarative YAML - generates code, uploads, adapts etc. for you. Can be customized using C++ and Python.

---

# Introducing Home Assitant (HA)

ESPHome can be fully integrated into HA.  We'll focus on getting HA up first, then move to ESPHome.

- Sponsored and commercial support available from https://www.nabucasa.com/
- Upstream project: https://www.home-assistant.io/

---

# Installation options

1. Home Assistant Operating System (recommended)
2. Home Assistant Container

For experienced users

3. Home Assistant Supervised
4. Home Assistant Core (Virtual python env)

HA-OS has supervised and everything preconfigured. And comes with direct support for Raspberry Pi and VMs.

---

# Installation continued

- [Home Asstant Blue](https://www.home-assistant.io/blue)
- [Raspberry Pi install image](https://www.home-assistant.io/installation/raspberrypi)
    - My home (real) use of HA uses a Pi4
- VMs
- Baremetal
- Need Linux - the HA-OS comes with a minimal Linux OS (everything is a container from then on)

---

# Our install for today's demo

https://www.home-assistant.io/installation/linux

- Using KVM/libvirt
- Creating a bridge device on the hypervisor (my laptop)
- Connecting old wifi AP for device connections
- dnsmasq and NAT to allow VM network to manage IPs and names

Already running - let's look very briefly at how it was configured.

---

# HA Features
Too many to really list them all ...

![bg right:40% 80%](ha-dashboard-example.png)

- Dashboards
- Support of hundreds of device types and cloud services
- Map rendering
- Location Tracking
- Alarms

---

# HA components

- Devices
- Entities
- Helpers
- Scenes
- Areas/zones
- Scripts/Blueprints
- Automations
- Add-ons

---

# Our add-ons

We'll be using a few addons

- [NodeRed](https://nodered.org/) - Integration made easy
- [EspHome](https://esphome.io/) - Configuration/running of ESP devices
- A few more just for demo like MQTT

Add-ons are in a separate container providing features separate from HA but AP integration into. You can absolutely make your own.

---

# HA concepts

- HA is a central integration hub
- It has a dashboard, but it offers a rich API allowing decentralized dashboards (like spread around your home)
- Strong integration with phone app allows for location tracking and geo-events. Ie. when not home, lock doors and sound alarm if door opens etc.
- House hold access to camera and other security information
- House hold access to AC controls, Alarm controls, water heater, backup power, solar system etc.

---

![bg 90%](live-demo.png)

---

# What's an ESP8266

![bg 60%](WeMos-d1-mini-Pin-out.webp)

---

# Connecting an ESP unit

- It's all about knowing what pin capabilities exist and making the connection
- Pins have more than one capability. But only one capability can be active at a time.
- Pins can be digital or analog, PWM, INPUT, OUTPUT, I2C, SPI and more
- We have breadboards and flexible connectors to test
- Not all pins have all features

---

# What to watch out for

- Pins have current limits
- Shorts - to be avoided or the MCU can be lost
- Pin limitations - some pins have controls for boot and programming built in
- 3.3V is all an ESP8266 understands. Give it 5v and it burns out very quickly and will not work.

---

# ESPHome programming steps

CLI (we won't be demoing this)

```
esphome wizard myesp.yaml
vim myesp.yaml
esphome compile myesp.yaml
esphome run myesp.yaml
```

Everything is manual. Including identifying the exact ESP version you have (there's a few hundred options). 

---

# ESPHome plugin steps
Must use Chrome to program via the browser!

- Access ESPHome web
    - Click Add new device
    - Click "First time Initialization"
    - Add WiFi information
    - Adopt device

---

# <!-- fit --> ESPHome plugin - program details

- Add specific content ("programming")
    - Sensors / binary_sensor
    - Switches
    - i2c/spi
    - number/text
- Use LAMBA for on-chip automation
    - Turn on LED when active or when failure
    - Sound alarm

---

# Run/compile ESPHome units

- If no errors, choose "run"
- Adopt to HA
- See sensors and other objects appear in HA
- Create dashboard feature to see content
    - Or view device dashboard for immediate access

---

# Today's demo has the following devices

- Stepper motor control - automatic blinds etc.
- Tempeature sensor - get room or outside temperature/humidity
- Door sensor with built in power relay control
- ESP32-CAM - camera feed using very cheap cameras

---

![bg 90%](live-demo.png)

---

# Thank you!

![bg](question-time.png)